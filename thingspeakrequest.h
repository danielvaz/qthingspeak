#ifndef THINGSPEAKREQUEST_H
#define THINGSPEAKREQUEST_H

#include <memory>

#include <QtQml>
#include <QDateTime>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QList>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QObject>

class ThingspeakRequest : public QObject
{
    Q_OBJECT
    Q_ENUMS(RequestState)
    Q_ENUMS(Averaging)
    Q_PROPERTY(int channel READ channel WRITE setChannel NOTIFY channelChanged)
    Q_PROPERTY(RequestState state READ state NOTIFY stateChanged)
    Q_PROPERTY(QDateTime requestingDay READ requestingDay NOTIFY requestingDayChanged)
    Q_PROPERTY(QJsonDocument thingspeakResponse READ thingspeakResponse NOTIFY thingspeakResponseChanged)
public:
    enum RequestState {
        Idle,
        GettingInstantaneousData,
        GettingCurrentDay,
        GettingDay,
        GettingPeriod
    };
    enum Averaging {
        NoAvg,
        Avg10Minutes,
        Avg15Minutes,
        Avg20Minutes,
        Avg30Minutes,
        AvgOneHour,
        AvgTwoHours,
        AvgHalfDay,
        AvgDaily
    };

    ThingspeakRequest(QObject* parent = Q_NULLPTR);
    //
    void instantaneousData();
    void requestDay(const QDateTime &day, Averaging avg = NoAvg);
    void requestCurrentDay(Averaging avg);
    void requestPeriod(const QDateTime &ini, const QDateTime &fini, Averaging avg);

    static int registerQml() {
        return qmlRegisterUncreatableType<ThingspeakRequest>("energy", 1, 0, "ThingspeakRequest", QStringLiteral("Used only by getting json documents"));
    }

    int channel() const
    {
        return m_channel;
    }

    RequestState state() const
    {
        return m_state;
    }

    QDateTime requestingDay() const
    {
        return m_requestingDay;
    }

    QJsonDocument thingspeakResponse() const
    {
        return m_thingspeakResponse;
    }

    void setChannel(int channel)
    {
        if (m_channel == channel)
            return;

        m_channel = channel;
        emit channelChanged(channel);
    }

    void setState(RequestState state)
    {
        if (m_state == state)
            return;

        m_state = state;
        emit stateChanged(state);
    }

    void setRequestingDay(QDateTime requestingDay)
    {
        if (m_requestingDay == requestingDay)
            return;

        m_requestingDay = requestingDay;
        emit requestingDayChanged(requestingDay);
    }

signals:
    void channelChanged(int channel);
    void isRequestingChanged(bool isRequesting);
    void stateChanged(RequestState state);
    void requestingDayChanged(QDateTime requestingDay);
    void thingspeakResponseChanged(const QJsonDocument &thingspeakResponse);

private:
    int m_channel;
    RequestState m_state;
    QDateTime m_requestingDay;
    QJsonDocument m_thingspeakResponse;
    QNetworkAccessManager *m_manager;
    QNetworkReply *m_reply;
    void appendDatastream();
    QString getPeriodUrl(const QDateTime &ini, const QDateTime &fini, Averaging avg=NoAvg);
    static QObject* singletonProvider(QQmlEngine*, QJSEngine*)
    {
        return new ThingspeakRequest;
    }
    QByteArray m_rawDatastream;
    //
    void dataReady();
    void connectToReply();
    QString averagingToString(Averaging avg)
    {
        switch (avg)
        {
        case Avg10Minutes:
            return QString("10");
        case Avg15Minutes:
            return QString("15");
        case Avg20Minutes:
            return QString("20");
        case Avg30Minutes:
            return QString("30");
        case AvgOneHour:
            return QString("60");
        case AvgTwoHours:
            return QString("240");
        case AvgHalfDay:
            return QString("720");
        case AvgDaily:
            return QString("1440");
        case NoAvg:
        default:
            return QString();
        }
    }
};

#endif // THINGSPEAKREQUEST_H
