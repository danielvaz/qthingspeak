#include "thingspeakrequest.h"

ThingspeakRequest::ThingspeakRequest(QObject *parent)
    : QObject(parent)
    , m_channel(-1)
    , m_state(Idle)
    , m_manager(new QNetworkAccessManager(this))
{
}

void ThingspeakRequest::instantaneousData()
{
    QString urlTemplate("https://api.thingspeak.com/channels/%1/feeds.json?results=1");
    QString urlBase = urlTemplate.arg(m_channel);
    m_reply = m_manager->get(QNetworkRequest(QUrl(urlBase)));
    connectToReply();
    setState(GettingInstantaneousData);
}

void ThingspeakRequest::requestDay(const QDateTime &day, ThingspeakRequest::Averaging avg)
{
    setRequestingDay(day);
    QDateTime midnight(day);
    QDateTime end(midnight.addDays(1));
    m_reply = m_manager->get(QNetworkRequest(QUrl(getPeriodUrl(midnight, end, avg))));
    connectToReply();
    setState(GettingDay);
}

void ThingspeakRequest::requestCurrentDay(ThingspeakRequest::Averaging avg)
{
    QDateTime midnight(QDate::currentDate());
    QDateTime now(QDateTime::currentDateTime());
    m_reply = m_manager->get(QNetworkRequest(QUrl(getPeriodUrl(midnight, now, avg))));
    connectToReply();
    setState(GettingCurrentDay);
}

void ThingspeakRequest::requestPeriod(const QDateTime &ini,
                                      const QDateTime &fini, Averaging avg)
{
    m_reply = m_manager->get(QNetworkRequest(QUrl(getPeriodUrl(ini, fini, avg))));
    connectToReply();
    setState(GettingPeriod);
}

void ThingspeakRequest::appendDatastream()
{
    m_rawDatastream.append(m_reply->readAll());
}

QString ThingspeakRequest::getPeriodUrl(const QDateTime &ini,
                                        const QDateTime &fini,
                                        Averaging avg)
{
    QString fmt("yyyy-MM-dd%20hh:mm:ss");
    QString start = ini.toUTC().toString(fmt);
    QString end = fini.toUTC().toString(fmt);
    QString urlTemplate("https://api.thingspeak.com/channels/%1/feeds.json?");
    QString urlBase = urlTemplate.arg(m_channel);
    urlBase += "start=" + start + "&end=" + end;
    if (avg != NoAvg)
    {
        urlBase += "&average=" + averagingToString(avg);
    }
    return urlBase;
}

void ThingspeakRequest::dataReady()
{
    setState(Idle);
    m_thingspeakResponse = QJsonDocument::fromJson( m_rawDatastream );
    emit thingspeakResponseChanged(m_thingspeakResponse);
}

void ThingspeakRequest::connectToReply()
{
    m_rawDatastream.clear();
    connect(m_reply, &QNetworkReply::readyRead,
            this, &ThingspeakRequest::appendDatastream);
    connect(m_reply, &QNetworkReply::finished,
            this, &ThingspeakRequest::dataReady);
}
