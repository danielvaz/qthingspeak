# README #

ThingSpeak is an Internet of Things (IoT) platform that lets you collect and store sensor data in the cloud and develop IoT applications. The ThingSpeak™ IoT platform provides apps that let you analyze and visualize your data in MATLAB®, and then act on the data. Sensor data can be sent to ThingSpeak from Arduino®, Raspberry Pi™, BeagleBone Black, and other hardware.

This is a Thingspeak API request wrapper using Qt infrastructure.

# References #
[Thingspeak](https://thingspeak.com/)

[Thingspeak Support](https://www.mathworks.com/help/thingspeak/)